﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.StringInfos
{
    public class JwtInfo
    {
        public const string Issuer = "http://localhost:5000";
        public const string Audience = "http://localhost:55668";
        public const string SecurityKey = "4e8b054c-059e-4e3d-9d40-5449a9e03a39";
        public const double Expires = 55;
    }
}
