﻿using FluentValidation;
using Isnet.Timesheet.Business.Concrete;
using Isnet.Timesheet.Business.Helpers.JWTHelper;
using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.Business.ValidationRules.FluentValidation;
using Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Repositories;
using Isnet.Timesheet.DataAccess.Interfaces;
using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.Containers.MicrosoftIoC
{
    public static class CustomIoCExtension
    {
        public static void AddDependencies(this IServiceCollection services)
        {
            services.AddScoped(typeof(IGenericDal<>), typeof(EfGenericRepository<>));
            services.AddScoped(typeof(IGenericService<>), typeof(GenericManager<>));

            services.AddScoped<IPersonnelService, PersonnelManager>();
            services.AddScoped<IPersonnelDal, EfPersonnelRepository>();
            services.AddScoped<IEventService, EventManager>();
            services.AddScoped<IEventDal, EfEventRepository>();
            services.AddScoped<IDirectorshipService, DirectorshipManager>();
            services.AddScoped<IDirectorshipDal, EfDirectorshipRepository>();


            services.AddScoped<IJwtService, JwtManager>();
            services.AddTransient<IValidator<PersonnelLoginDto>, PersonnelLoginValidator>();
            services.AddTransient<IValidator<PersonnelLoginDto>, PersonnelLoginValidator>();

        }
    }
}
