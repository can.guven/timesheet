﻿using Isnet.Timesheet.DTO.DTOs.EventDTOs;
using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.Interfaces
{
    public interface IEventService :IGenericService<Event>
    {
        Task<List<EventListPDto>> GetAllWithFilterAsync(EventFilterDto eventFilterDto);
        Task<List<Event>> GetAllByPersonnelAsync(int personnelId);
        Task<List<Event>> GetAllByDirectorshipAsync(int directorshipId);
        Task<List<EventListPDto>> GetJoinsAll();
        Task<EventListPDto> GetJoinedById(int eventId);
        Task<List<EventListPDto>> GetJoinsAllByPersonnel(int personnelId);
        Task<List<EventListPDto>> GetJoinsAllByDirectorships(int directorshipId);

        Task<Event> AddEvent(Event addEvent);


    }
}
