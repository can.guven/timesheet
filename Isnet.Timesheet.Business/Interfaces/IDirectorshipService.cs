﻿using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.Interfaces
{
    public interface IDirectorshipService : IGenericService<Directorship>
    {
        Task<List<Directorship>> FindByContainNameAsync(string name);
    }
}
