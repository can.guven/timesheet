﻿using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.Interfaces
{
    public interface IPersonnelService : IGenericService<Personnel>
    {
        Task<Personnel> CheckUserAsync(PersonnelLoginDto personnelLoginDto);
        Task<Personnel> FindByUsernameAsync(string name);
        Task<Personnel> FindByNameAsync(string userName);
        Task AddPersonnelAsync(Personnel personnel);
        Task<List<Personnel>> FindByContainNameAsync(string name);
        Task<List<Personnel>> GetNoEventPersonnel();

    }
}
