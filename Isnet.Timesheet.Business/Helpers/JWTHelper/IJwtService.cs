﻿using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Isnet.Timesheet.Business.Helpers.JWTHelper
{
    public interface IJwtService
    {
        JwtToken GenerateJwt(Personnel personnel);
    }
}
