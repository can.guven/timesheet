﻿using Isnet.Timesheet.Business.StringInfos;
using Isnet.Timesheet.Entities.Concrete;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.Helpers.JWTHelper
{
    public class JwtManager : IJwtService
    {
        public JwtToken GenerateJwt(Personnel personnel)
        {   
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtInfo.SecurityKey));

            SigningCredentials signingCredentials = new SigningCredentials(securityKey,SecurityAlgorithms.HmacSha256);

            JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(issuer:JwtInfo.Issuer,audience:JwtInfo.Audience,
                claims:SetClaims(personnel),notBefore:DateTime.Now,expires:DateTime.Now.AddMinutes(JwtInfo.Expires),
                signingCredentials:signingCredentials);

            JwtToken jwtToken = new JwtToken();

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

            jwtToken.Token = handler.WriteToken(jwtSecurityToken);

            return jwtToken;
        }
        private List<Claim> SetClaims(Personnel personnel)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, personnel.Username));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, personnel.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Role, personnel.Role));
            claims.Add(new Claim("DirectorshipId", personnel.DirectorshipID.ToString()));
            return claims;
        }
    }
}
