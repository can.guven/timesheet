﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.Helpers.JWTHelper
{
    public class JwtToken
    {
        public string Token { get; set; }
    }
}
