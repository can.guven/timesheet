﻿using FluentValidation;
using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.ValidationRules.FluentValidation
{
    public class PersonnelLoginValidator : AbstractValidator<PersonnelLoginDto>
    {
        public PersonnelLoginValidator()
        {
            RuleFor(I => I.Username).NotEmpty().WithMessage("Kullanıcı adı boş geçilemez...");
            RuleFor(I => I.Password).NotEmpty().WithMessage("Şifre boş geçilemez...");
        }
    }
}
