﻿using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DataAccess.Interfaces;
using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.Concrete
{
    public class DirectorshipManager : GenericManager<Directorship>, IDirectorshipService
    {
        private readonly IGenericDal<Directorship> _genericDal;
        private readonly IDirectorshipDal _directorshipDal;
        public DirectorshipManager(IGenericDal<Directorship> genericDal, IDirectorshipDal directorshipDal) : base(genericDal)
        {
            _genericDal = genericDal;
            _directorshipDal = directorshipDal;
        }
        public async Task<List<Directorship>> FindByContainNameAsync(string name)
        {
            return await _genericDal.GetAllAsync(I => I.Name.ToLower().Contains(name));
        }
    }
}
