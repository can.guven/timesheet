﻿using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DataAccess.Interfaces;
using Isnet.Timesheet.DTO.DTOs.EventDTOs;
using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Business.Concrete
{
    public class EventManager : GenericManager<Event>, IEventService
    {
        private readonly IGenericDal<Event> _genericDal;

        private readonly IEventDal _eventDal;
        public EventManager(IGenericDal<Event> genericDal, IEventDal eventDal) : base(genericDal)
        {
            _genericDal = genericDal;
            _eventDal = eventDal;
        }
        public async Task<List<Event>> GetAllByPersonnelAsync(int personnelId)
        {
            return await _genericDal.GetAllAsync(I => I.PersonnelID == personnelId);
        }
        public async Task<List<Event>> GetAllByDirectorshipAsync(int directorshipId)
        {
            return await _genericDal.GetAllAsync(I => I.DirectorshipID == directorshipId);
        }
        public async Task<List<EventListPDto>> GetAllWithFilterAsync(EventFilterDto eventFilterDto)
        {
            return await _eventDal.GetAllWithFilterAsync(eventFilterDto);
        }

        public async Task<List<EventListPDto>> GetJoinsAll()
        {
            return await _eventDal.GetJoinsAll();
        }

        public async Task<List<EventListPDto>> GetJoinsAllByPersonnel(int personnelId)
        {
            return await _eventDal.GetJoinsWithCondition(I => I.PersonnelID == personnelId);
        }
        public async Task<List<EventListPDto>> GetJoinsAllByDirectorships(int directorshipId)
        {

            return await _eventDal.GetJoinsWithCondition(I => I.DirectorshipID == directorshipId);
        }

        public async Task<EventListPDto> GetJoinedById(int eventId)
        {

            return await _eventDal.GetJoinedWithCondition(I => I.Id == eventId);
        }

        public async Task<Event> AddEvent(Event addEvent)
        {
            return await _eventDal.AddEvent(addEvent);
        }
    }
}
