﻿using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DataAccess.Interfaces;
using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using Isnet.Timesheet.Entities.Concrete;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Isnet.Timesheet.Business.Concrete
{
    public class PersonnelManager : GenericManager<Personnel>, IPersonnelService
    {   private readonly IGenericDal<Personnel> _genericDal;
        private readonly IPersonnelDal _personnelDal;
        public PersonnelManager(IGenericDal<Personnel> genericDal, IPersonnelDal personnelDal) : base(genericDal)
        {
            _genericDal = genericDal;
            _personnelDal = personnelDal;
        }

        public async Task AddPersonnelAsync(Personnel personnel)
        {
            var passwordHasher = new PasswordHasher<Personnel>();
            var hashedPassword = passwordHasher.HashPassword(personnel, personnel.Password);
            personnel.Password = hashedPassword;
            await _genericDal.AddAsync(personnel);
        }

        public  async Task<Personnel> CheckUserAsync(PersonnelLoginDto personnelLoginDto)
        {
            return await _genericDal.GetAsync(I => I.Username == personnelLoginDto.Username
            && I.Password == personnelLoginDto.Password);
        }

        public async Task<Personnel> FindByUsernameAsync(string userName)
        {
            return await _genericDal.GetAsync(I => I.Username == userName);
        }

        public async Task<Personnel> FindByNameAsync(string name)
        {
            return await _genericDal.GetAsync(I => I.Name == name);
        }
        public async Task<List<Personnel>> FindByContainNameAsync(string name)
        {
            return await _genericDal.GetAllAsync(I => I.Name.ToLower().Contains(name));
        }
        public async Task<List<Personnel>> GetNoEventPersonnel()
        {
            return await _personnelDal.GetNoEventPersonnel();
        }
    }
}
