﻿using Isnet.Timesheet.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Entities.Concrete
{
    public class Personnel : ITable
    {
        public int Id { get; set; }
        public int DirectorshipID { get; set; }
        public virtual Directorship Directorship { get; set; }
        public ICollection<Event> Events { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }


    }
}
