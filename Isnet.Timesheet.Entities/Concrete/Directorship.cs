﻿using Isnet.Timesheet.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Entities.Concrete
{
    public class Directorship : ITable
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public ICollection<Personnel> Personnels { get; set; }
        public ICollection<Event> Events { get; set; }
    }
}
