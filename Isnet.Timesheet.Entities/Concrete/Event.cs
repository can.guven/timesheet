﻿using Isnet.Timesheet.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.Entities.Concrete
{
    public class Event : ITable
    {
        public int Id { get; set; }
        public int PersonnelID { get; set; }
        public Personnel Personnel { get; set; }
        public int DirectorshipID { get; set; }
        public Directorship Directorship { get; set; }
        public string CustomerName { get; set; }
        public string Category { get; set; }
        public string Title { get; set; }
        public string CompletedDesc { get; set; }
        public bool Status { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
