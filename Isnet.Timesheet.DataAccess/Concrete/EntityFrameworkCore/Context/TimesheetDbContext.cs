﻿using Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Mapping;
using Isnet.Timesheet.Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Context
{
    public class TimesheetDbContext : DbContext
    {
        public DbSet<Directorship> Directorships { get; set; }
        public DbSet<Personnel> Personnels { get; set; }
        public DbSet<Event> Events { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DirectorshipMap());
            modelBuilder.ApplyConfiguration(new EventMap());
            modelBuilder.ApplyConfiguration(new PersonnelMap());
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
          => optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Username=postgres;Password=isnet;Database=TimesheetApiDB;");
    }
}
