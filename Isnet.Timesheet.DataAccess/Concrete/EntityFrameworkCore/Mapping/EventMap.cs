﻿using Isnet.Timesheet.Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Mapping
{
    public class EventMap : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.HasKey(I => I.Id);
            builder.Property(I => I.CustomerName).IsRequired();
            builder.Property(I => I.Category).IsRequired();
            builder.Property(I => I.Title).IsRequired();
            builder.Property(I => I.StartDate).IsRequired();
        }
    }
}
