﻿using Isnet.Timesheet.Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Mapping
{
    public class DirectorshipMap : IEntityTypeConfiguration<Directorship>
    {
        public void Configure(EntityTypeBuilder<Directorship> builder)
        {
            builder.HasKey(I => I.Id);
            builder.Property(I => I.Id).UseIdentityColumn();
            builder.Property(I => I.Name).HasMaxLength(50).IsRequired();
            builder.Property(I => I.Code).HasMaxLength(20).IsRequired();
            builder.HasMany(I => I.Personnels).WithOne(I => I.Directorship).HasForeignKey(I => I.DirectorshipID);
            builder.HasMany(I => I.Events).WithOne(I => I.Directorship).HasForeignKey(I => I.DirectorshipID);
        } 
    }
}
