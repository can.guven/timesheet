﻿using Isnet.Timesheet.Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Mapping
{
    public class PersonnelMap : IEntityTypeConfiguration<Personnel>
    {
        public void Configure(EntityTypeBuilder<Personnel> builder)
        {
            builder.HasKey(I => I.Id);
            builder.Property(I => I.Id).UseIdentityColumn();
            builder.Property(I => I.Name).HasMaxLength(50).IsRequired();
            builder.Property(I => I.Username).HasMaxLength(50).IsRequired();
            builder.HasIndex(I => I.Username).IsUnique();
            builder.Property(I => I.Password).IsRequired();
            builder.Property(I => I.Role).HasMaxLength(30).IsRequired();
            builder.HasMany(I => I.Events).WithOne(I => I.Personnel).HasForeignKey(I => I.PersonnelID);
        }
    }
}
