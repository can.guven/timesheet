﻿using Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Context;
using Isnet.Timesheet.DataAccess.Interfaces;
using Isnet.Timesheet.Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Repositories
{
    public class EfPersonnelRepository : EfGenericRepository<Personnel>, IPersonnelDal
    {
        public async Task<List<Personnel>> GetNoEventPersonnel()
        {
            using var context = new TimesheetDbContext();
            var personnels = await context.Personnels.Where(I => I.Role.Equals("User") && 
            I.Events.Where(K => K.EndDate>=DateTime.Today).Count()==0
            && DateTime.Today.DayOfWeek != DayOfWeek.Sunday
            && DateTime.Today.DayOfWeek != DayOfWeek.Saturday
            ).ToListAsync();
            return personnels;
        }
    }
}
