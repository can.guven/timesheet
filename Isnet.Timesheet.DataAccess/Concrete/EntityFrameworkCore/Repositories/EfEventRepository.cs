﻿using AutoMapper.Execution;
using Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Context;
using Isnet.Timesheet.DataAccess.Interfaces;
using Isnet.Timesheet.DTO.DTOs.EventDTOs;
using Isnet.Timesheet.Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Repositories
{
    public class EfEventRepository : EfGenericRepository<Event>, IEventDal
    {
        public async Task<List<EventListPDto>> GetAllWithFilterAsync(EventFilterDto eventFilterDto)
        {
            using var context = new TimesheetDbContext();

            var events = context.Events.AsQueryable();
            if (!string.IsNullOrEmpty(eventFilterDto.Category))
            {
                events = events.Where(I => I.Category.Contains(eventFilterDto.Category));
            }
            if (!string.IsNullOrEmpty(eventFilterDto.CustomerName))
            {
                events = events.Where(I => I.CustomerName.Contains(eventFilterDto.CustomerName));
            }
            if (eventFilterDto.StartDate != DateTimeOffset.MinValue)
            {
                events = events.Where(I => I.StartDate>=eventFilterDto.StartDate);
            }
            if (eventFilterDto.EndDate != DateTimeOffset.MinValue)
            {
                events = events.Where(I => I.EndDate <= eventFilterDto.EndDate);
            }
            if(eventFilterDto.PersonnelID != 0)
            {
                events = events.Where(I => I.PersonnelID == eventFilterDto.PersonnelID);
            }
            if(eventFilterDto.DirectorshipID != 0)
            {
                events = events.Where(I => I.DirectorshipID == eventFilterDto.DirectorshipID);
            }

            List<EventListPDto> listEvents = await events.Select(x
                 => new EventListPDto()
                 {
                     Id = x.Id,
                     PersonnelID = x.PersonnelID,
                     PersonnelName = x.Personnel.Name,
                     DirectorshipName = x.Directorship.Name,
                     DirectorshipID = x.DirectorshipID,
                     CustomerName = x.CustomerName,
                     Category = x.Category,
                     Title = x.Title,
                     CompletedDesc = x.CompletedDesc,
                     Status = x.Status,
                     StartDate = x.StartDate,
                     EndDate = x.EndDate
                 })
                .OrderByDescending(I => I.EndDate).ToListAsync();

            return listEvents;

        }
        public async Task<List<EventListPDto>> GetJoinsAll()
        {
            using var context = new TimesheetDbContext();
            List<EventListPDto> events = await context.Events.Select(x
                => new EventListPDto()
                {
                    Id = x.Id,
                    PersonnelID=x.PersonnelID,
                    PersonnelName = x.Personnel.Name,
                    DirectorshipName = x.Directorship.Name,
                    DirectorshipID=x.DirectorshipID,
                    CustomerName=x.CustomerName,
                    Category=x.Category,
                    Title = x.Title,
                    CompletedDesc = x.CompletedDesc,
                    Status = x.Status,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate
                })
                .OrderByDescending(I=>I.EndDate).ToListAsync();

            return events;
        }

        public async Task<List<EventListPDto>> GetJoinsWithCondition(Expression<Func<Event, bool>> filter)
        {
            using var context = new TimesheetDbContext();
            List<EventListPDto> events = await context.Events.Where(filter).Select(x
                => new EventListPDto()
                {
                    Id = x.Id,
                    PersonnelID = x.PersonnelID,
                    PersonnelName = x.Personnel.Name,
                    DirectorshipName = x.Directorship.Name,
                    DirectorshipID = x.DirectorshipID,
                    CustomerName = x.CustomerName,
                    Category = x.Category,
                    Title = x.Title,
                    CompletedDesc = x.CompletedDesc,
                    Status = x.Status,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate
                })
                .OrderByDescending(I => I.EndDate).ToListAsync();

            return events;
        }
        public async Task<EventListPDto> GetJoinedWithCondition(Expression<Func<Event, bool>> filter)
        {
            using var context = new TimesheetDbContext();
            EventListPDto oneEvent = await context.Events.Where(filter).Select(x
                => new EventListPDto()
                {
                    Id = x.Id,
                    PersonnelID = x.PersonnelID,
                    PersonnelName = x.Personnel.Name,
                    DirectorshipName = x.Directorship.Name,
                    DirectorshipID = x.DirectorshipID,
                    CustomerName = x.CustomerName,
                    Category = x.Category,
                    Title = x.Title,
                    CompletedDesc = x.CompletedDesc,
                    Status = x.Status,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate
                }).FirstOrDefaultAsync();

            return oneEvent;
        }

        public async Task<Event> AddEvent(Event addEvent)
        {
            using var context = new TimesheetDbContext();
            await context.Events.AddAsync(addEvent);
            await context.SaveChangesAsync();
            return addEvent;
        }
    }
}
