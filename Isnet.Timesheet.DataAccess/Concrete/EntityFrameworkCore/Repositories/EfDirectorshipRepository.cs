﻿using Isnet.Timesheet.DataAccess.Interfaces;
using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DataAccess.Concrete.EntityFrameworkCore.Repositories
{
    public class EfDirectorshipRepository : EfGenericRepository<Directorship>, IDirectorshipDal
    {
    }
}
