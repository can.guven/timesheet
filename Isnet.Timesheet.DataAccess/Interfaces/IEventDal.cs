﻿using Isnet.Timesheet.DTO.DTOs.EventDTOs;
using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace Isnet.Timesheet.DataAccess.Interfaces
{
    public interface IEventDal : IGenericDal<Event>
    {
        Task<List<EventListPDto>> GetAllWithFilterAsync(EventFilterDto eventFilterDto);
        Task<List<EventListPDto>> GetJoinsAll();
        Task<List<EventListPDto>> GetJoinsWithCondition(Expression<Func<Event, bool>> filter);
        Task<EventListPDto> GetJoinedWithCondition(Expression<Func<Event, bool>> filter);
        Task<Event> AddEvent(Event eventAddDto);
    }
}
