﻿using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DataAccess.Interfaces
{
    public interface IDirectorshipDal : IGenericDal<Directorship>
    {
    }
}
