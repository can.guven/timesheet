﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DTO.DTOs.DirectorshipDTOs
{
    public class DirectorshipAddDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
