﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DTO.DTOs.EventDTOs
{
    public class EventListPDto
    {
        public int Id { get; set; }
        public string PersonnelName { get; set; }
        public int PersonnelID { get; set; }
        public string DirectorshipName { get; set; }
        public int DirectorshipID { get; set; }
        public string CustomerName { get; set; }
        public string Category { get; set; }
        public string Title { get; set; }
        public string CompletedDesc { get; set; }
        public bool Status { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}
