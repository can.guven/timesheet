﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DTO.DTOs.EventDTOs
{
    public class EventFilterDto
    {
        public int PersonnelID { get; set; }
        public int DirectorshipID { get; set; }
        public string CustomerName { get; set; }
        public string Category{ get; set; }
        public bool Status { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}
