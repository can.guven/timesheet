﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.DTO.DTOs.PersonnelDTOs
{
    public class PersonnelLoginDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
