﻿using AutoMapper;
using Isnet.Timesheet.DTO.DTOs.DirectorshipDTOs;
using Isnet.Timesheet.DTO.DTOs.EventDTOs;
using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using Isnet.Timesheet.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Isnet.Timesheet.WebApi.Mapping.AutoMapperProfile
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<PersonnelListDto, Personnel>();
            CreateMap<Personnel, PersonnelListDto>();

            CreateMap<PersonnelAddDto, Personnel>();
            CreateMap<Personnel, PersonnelAddDto>();

            CreateMap<PersonnelAddDto, PersonnelLoginDto>();
            CreateMap<PersonnelLoginDto, PersonnelAddDto>();



            CreateMap<EventAddDto, Event>();
            CreateMap<Event, EventAddDto>();

            CreateMap<EventListDto, Event>();
            CreateMap<Event, EventListDto>();

            CreateMap<EventUpdateDto, Event>();
            CreateMap<Event, EventUpdateDto>();

            CreateMap<Event, EventFilterDto>();
            CreateMap<EventFilterDto, Event>();

            CreateMap<Event, EventListPDto>();
            CreateMap<EventListPDto, Event>();

            CreateMap<DirectorshipListDto, Directorship>();
            CreateMap<Directorship, DirectorshipListDto>();

            CreateMap<DirectorshipAddDto, Directorship>();
            CreateMap<Directorship, DirectorshipAddDto>();


        }

    }
}
