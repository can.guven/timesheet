﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using Isnet.Timesheet.Entities.Concrete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace Isnet.Timesheet.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PersonnelsController : ControllerBase
    {
        private readonly IPersonnelService _personnelService;
        private readonly IMapper _mapper;
        public PersonnelsController(IPersonnelService personnelService, IMapper mapper)
        {
            _personnelService = personnelService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(_mapper.Map<List<PersonnelListDto>>(await _personnelService.GetAllAsync()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(_mapper.Map<PersonnelListDto>(await _personnelService.FindByIdAsync(id)));
        }

        [HttpGet("by-name/{name}")]
        public async Task<IActionResult> GetByName(string name)
        {
            return Ok(_mapper.Map<PersonnelListDto>(await _personnelService.FindByNameAsync(name)));
        }
        [HttpGet("by-username/{username}")]
        public async Task<IActionResult> GetByUsername(string username)
        {
            return Ok(_mapper.Map<PersonnelListDto>(await _personnelService.FindByUsernameAsync(username)));
        }

        [HttpPost]
        public async Task<IActionResult> Create(PersonnelAddDto personnelAddDto)
        {
            var checkuser = await _personnelService.FindByUsernameAsync(personnelAddDto.Username);
            if (checkuser != null)
            {
                return BadRequest("User exist");
            }
            await _personnelService.AddPersonnelAsync(_mapper.Map<Personnel>(personnelAddDto));
            return Created("", personnelAddDto);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var personnel = await _personnelService.FindByIdAsync(id);
            if (personnel == null)
            {
                return new NotFoundResult();
            }

            await _personnelService.RemoveAsync(new Personnel { Id = id });
                return NoContent();
        }
    }
}
