﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Isnet.Timesheet.Business.Helpers.JWTHelper;
using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using Isnet.Timesheet.Entities.Concrete;
using Isnet.Timesheet.WebApi.CustomFilters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;


namespace Isnet.Timesheet.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IPersonnelService _personnelService;
        private readonly IJwtService _jwtService;
        private readonly IMapper _mapper;
        public AuthController(IPersonnelService personnelService, IJwtService jwtService, IMapper mapper)
        {
            _personnelService = personnelService;
            _jwtService = jwtService;
            _mapper = mapper;
        }
        [HttpPost("[action]")]
        [ValidModel]
        public async Task<IActionResult> Login(PersonnelLoginDto personnelLoginDto)
        {   
            var personnel = await _personnelService.FindByUsernameAsync(personnelLoginDto.Username);
            var passwordHasher = new PasswordHasher<Personnel>();
            if (personnel != null)
            {
                bool verified = false;
                var result = passwordHasher.VerifyHashedPassword(personnel, personnel.Password, personnelLoginDto.Password);
                if (result == PasswordVerificationResult.Success) verified = true;
                else if (result == PasswordVerificationResult.SuccessRehashNeeded) verified = true;
                else if (result == PasswordVerificationResult.Failed) verified = false;
                if (verified)
                {
                    return Created("", _jwtService.GenerateJwt(personnel));
                }
                return BadRequest("Kullanıcı adı veya şifre hatalı");
            }
            return BadRequest("Kullanıcı adı veya şifre hatalı");
        }
        [HttpGet("[action]")]
        [Authorize]
        public async Task<IActionResult> ActiveUser()
        {
           var personnel = await _personnelService.FindByUsernameAsync(User.Identity.Name);
            if (personnel == null)
            {
                return BadRequest();
            }
            return Ok(_mapper.Map<PersonnelListDto>(personnel));
        }
    }
}
