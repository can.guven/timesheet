﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DTO.DTOs.EventDTOs;
using Isnet.Timesheet.Entities.Concrete;
using Isnet.Timesheet.WebApi.CustomHandlers;
using Isnet.Timesheet.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Isnet.Timesheet.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EventsController : ControllerBase
    {
        private readonly IEventService _eventService;
        private readonly IMapper _mapper;
        private readonly IAuthorizationService _authorizationService;
        public EventsController(IEventService eventService,
            IMapper mapper,
            IAuthorizationService authorizationService)
        {
            _eventService = eventService;
            _authorizationService = authorizationService;
            _mapper = mapper;
        }
        [HttpPost]
        public async Task<IActionResult> Create(EventAddDto eventAddDto)
        {
            var eventvar = _mapper.Map<Event>(eventAddDto);
            var authorizationResult = await _authorizationService
           .AuthorizeAsync(User, eventvar, Operations.Read);
            if (authorizationResult.Succeeded)
            {
                var savedEvent = await _eventService.AddEvent(_mapper.Map<Event>(eventAddDto));
                return Created("", savedEvent);
            }
            else
            {
                return new ChallengeResult();
            }
        }

        [HttpGet]
        [Authorize(Roles = Role.SuperAdmin)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _eventService.GetJoinsAll());
        }

        [HttpPut]
        public async Task<IActionResult> Update(EventUpdateDto eventUpdateDto)
        {
            var eventVar = await _eventService.FindByIdAsync(eventUpdateDto.Id);

            var authorizationResult = await _authorizationService
           .AuthorizeAsync(User, eventVar, Operations.Read);

            if (authorizationResult.Succeeded)
            {
                await _eventService.UpdateAsync(_mapper.Map<Event>(eventUpdateDto));
                return NoContent();
            }
            else
            {
                return new ChallengeResult();
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(_mapper.Map<EventListPDto>(await _eventService.GetJoinedById(id)));
        }

       [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var eventVar = await _eventService.FindByIdAsync(id);
            if(eventVar == null)
            {
                return new NotFoundResult();
            }

            var authorizationResult = await _authorizationService
           .AuthorizeAsync(User, eventVar, Operations.Read);

            if (authorizationResult.Succeeded)
            {
                await _eventService.RemoveAsync(new Event { Id = id });
                return NoContent();
            }
            else
            {
                return new ChallengeResult();
            }
        }

        [HttpGet("by-personnel/{id}")]
        public async Task<IActionResult> GetAllByPersonnelAsync(int id)
        {
            return Ok(await _eventService.GetJoinsAllByPersonnel(id));
        }

        [HttpGet("by-directorship/{id}")]
        public async Task<IActionResult> GetAllByDirectorshipAsync(int id)
        {
            return Ok(await _eventService.GetJoinsAllByDirectorships(id));
        }

        
    }
}
