﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DTO.DTOs.DirectorshipDTOs;
using Isnet.Timesheet.Entities.Concrete;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Isnet.Timesheet.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DirectorshipsController : ControllerBase
    {
        private readonly IDirectorshipService _directorshipService;
        private readonly IMapper _mapper;
        public DirectorshipsController(IDirectorshipService directorshipService, IMapper mapper)
        {
            _directorshipService = directorshipService;
            _mapper = mapper;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(_mapper.Map<List<DirectorshipListDto>>(await _directorshipService.GetAllAsync()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(_mapper.Map<DirectorshipListDto>(await _directorshipService.FindByIdAsync(id)));
        }

        [HttpPost]
        public async Task<IActionResult> Create(DirectorshipAddDto directorshipAddDto)
        {
            await _directorshipService.AddAsync(_mapper.Map<Directorship>(directorshipAddDto));
            return Created("", directorshipAddDto);
        }
        [HttpPut]
        public async Task<IActionResult> Update(DirectorshipUpdateDto directorshipUpdateDto)
        {
            var eventData = _mapper.Map<Directorship>(await _directorshipService.FindByIdAsync(directorshipUpdateDto.Id));
            if (eventData == null)
            {
                return BadRequest();
            }
            await _directorshipService.UpdateAsync(_mapper.Map<Directorship>(directorshipUpdateDto));
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var directorship = await _directorshipService.FindByIdAsync(id);
            if (directorship == null)
            {
                return new NotFoundResult();
            }

            await _directorshipService.RemoveAsync(new Directorship { Id = id });
            return NoContent();
        }


    }
}
