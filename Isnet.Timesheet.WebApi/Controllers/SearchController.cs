﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DTO.DTOs.DirectorshipDTOs;
using Isnet.Timesheet.DTO.DTOs.EventDTOs;
using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Isnet.Timesheet.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SearchController : ControllerBase
    {
        private readonly IPersonnelService _personnelService;
        private readonly IEventService _eventService;
        private readonly IDirectorshipService _directorshipService;
        private readonly IMapper _mapper;
        public SearchController(IPersonnelService personnelService, IMapper mapper,
            IDirectorshipService directorshipService, IEventService eventService)
        {
            _personnelService = personnelService;
            _directorshipService = directorshipService;
            _eventService = eventService;
            _mapper = mapper;
        }
        [HttpGet("personnel")]
        public async Task<IActionResult> SearchPersonnel([FromQuery] string query)
        {
            return Ok(_mapper.Map<List<PersonnelListDto>>(await _personnelService.FindByContainNameAsync(query.ToLower())));
        }
        [HttpGet("directorship")]
        public async Task<IActionResult> SearchDirectorship([FromQuery] string query)
        {
            return Ok(_mapper.Map<List<DirectorshipListDto>>(await _directorshipService.FindByContainNameAsync(query.ToLower())));
        }
        [HttpGet("event")]
        public async Task<IActionResult> SearchEvent([FromQuery] EventFilterDto eventFilterDto)
        {
            return Ok(await _eventService.GetAllWithFilterAsync(eventFilterDto));
        }
    }
}
