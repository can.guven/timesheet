﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Isnet.Timesheet.WebApi.Models
{
    public static class Role
    {
        public const string Admin = "Admin";
        public const string User = "User";
        public const string SuperAdmin = "Super Admin";
    }
}
