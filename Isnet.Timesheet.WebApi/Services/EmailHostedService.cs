﻿using AutoMapper;
using ClosedXML.Excel;
using Isnet.Timesheet.Business.Interfaces;
using Isnet.Timesheet.DTO.DTOs.PersonnelDTOs;
using Isnet.Timesheet.WebApi.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
namespace Isnet.Timesheet.WebApi.Services
{
    public class EmailHostedService : IHostedService, IDisposable
    {
        private int executionCount = 0;
        private readonly ILogger<EmailHostedService> _logger;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IMapper _mapper;
        private Timer _timer;

        public EmailHostedService(ILogger<EmailHostedService> logger,IMapper mapper, IServiceScopeFactory scopeFactory)
        {
            _logger = logger;
            _scopeFactory = scopeFactory;
            _mapper = mapper;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Email Hosted Service running.");
            TimeSpan interval = TimeSpan.FromHours(23);
            var nextRunTime = DateTime.Today.AddDays(1);
            var curTime = DateTime.Now;
            var firstInterval = nextRunTime.Subtract(curTime);

            Action action = () =>
            {
                var t1 = Task.Delay(firstInterval);
                t1.Wait();
                DoWork(null);
                _timer = new Timer(
                    DoWork,
                    null,
                    TimeSpan.Zero,
                    interval
                );
            };

            Task.Run(action);
            
            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        {
            var count = Interlocked.Increment(ref executionCount);
            using (var scope = _scopeFactory.CreateScope())
            {
                var _personnelService = scope.ServiceProvider.GetRequiredService<IPersonnelService>();
                var _mailService = scope.ServiceProvider.GetRequiredService<IMailService>(); ;
                var personnels = _mapper.Map<List<PersonnelListDto>>(await _personnelService.GetNoEventPersonnel());
                if (personnels.Count() > 0)
                {
                    using var workbook = new XLWorkbook();

                    var worksheet = workbook.Worksheets.Add("Personeller");
                    var currentRow = 1;
                    worksheet.Cell(currentRow, 1).Value = "Personel ID";
                    worksheet.Cell(currentRow, 2).Value = "Personel Ad";
                    worksheet.Cell(currentRow, 3).Value = "Müdürlük ID";
                    foreach (var personnel in personnels)
                    {
                        currentRow++;
                        worksheet.Cell(currentRow, 1).Value = personnel.Id;
                        worksheet.Cell(currentRow, 2).Value = personnel.Name;
                        worksheet.Cell(currentRow, 3).Value = personnel.DirectorshipID;
                    }
                    MailRequest mailRequest = new MailRequest();
                    mailRequest.Subject = DateTime.Today.ToShortDateString() + " Tarihinde İş Girişi Yapmayan Personeller";
                    mailRequest.ToEmail = "codecanguven@gmail.com";
                    mailRequest.Body = "İş takvimine "+ DateTime.Today.ToShortDateString() + " tarihinde iş girişi yapmayan personeller ektedir...";
                    using (var memoryStream = new MemoryStream())
                    {
                        workbook.SaveAs(memoryStream);
                        byte[] byteArr = memoryStream.ToArray();
                        MemoryStream excelStream = new MemoryStream(byteArr, true);
                        excelStream.Write(byteArr, 0, byteArr.Length);
                        excelStream.Position = 0;
                        mailRequest.Attachments.Add(new Attachment(excelStream, "is_girisi_yapmayanlar.xlsx"));
                        await _mailService.SendEmailAsync(mailRequest);
                        memoryStream.Close();
                        excelStream.Close();
                    }
                }
            }
               
            _logger.LogInformation(
            "Email Hosted Service is working. Count: {Count}", count);
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Email Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
